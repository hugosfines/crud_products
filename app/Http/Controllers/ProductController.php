<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Models\Product;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        //
    }

    public function store(ProductRequest $request)
    {
        DB::beginTransaction();
        try{
            $product = Product::create([
                'name' => $request->name,
                'ref' => $request->ref,
                'price' => $request->price,
                'amount' => $request->amount ? $request->amount : 0,
                'description' => $request->description,
            ]);
            $file = $request->image;
            if($file) {
                //$path = storage_path('app/file');
                $path = public_path('img/products');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                $file->move($path, $fileName);
                $product->image = $fileName;
            }
            $product->save();
            DB::commit();
            return response()->json(['statusText' => 'ok', 'message' => 'Producto guardado con exito.'], 200);
        }catch(Exception $e) {
            DB::rollback();
            return response()->json(['errors' => ['error' => ['Ha ocurrido un error.']]], 423);
        }
    }

    public function update(ProductRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $product = Product::findOrFail($id);
            $product->name = $request->name;
            $product->ref = $request->ref;
            $product->price = $request->price;
            $product->amount = $request->amount ? $request->amount : 0;
            $product->description = $request->description;
            $file = $request->image;
            if($file) {
                //$path = storage_path('app/file');
                $path = public_path('img/products');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                $file->move($path, $fileName);
                $product->image = $fileName;
            }
            $product->save();
            DB::commit();
            return response()->json(['statusText' => 'ok', 'message' => 'Producto actualizado con exito.'], 200);
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['errors' => ['error' => ['Ha ocurrido un error.']]], 423);
        }
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json(['statusText' => 'ok', 'message' => 'Producto eliminado con exito.'], 200);
    }
}

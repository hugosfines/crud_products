<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'string'],
            'ref' => ['required', 'string', 'max:50'],
            'price' => ['required', 'numeric', 'regex:/^-?[0-9]+(?:.[0-9]{1,2})?$/'],
            'amount' => ['integer'],
            'description' => ['nullable', 'string', 'max:250'],
        ];
        if ($this->image)
            $rules = array_merge($rules, ['image' => 'mimes:jpg,jpeg,png|max:10240']);

        return $rules;
    }
}
